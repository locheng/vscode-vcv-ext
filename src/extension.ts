import {
  ExtensionContext,
  languages,
  workspace,
  CompletionItemKind,
  CompletionItem,
  Uri,
} from "vscode";
import { languageTypeMap } from "./utils";
import { LanguageInfo, LanguageMap } from "./types";

async function readFileContent(uri: Uri) {
  const array = await workspace.fs.readFile(uri);
  const nums: number[] = [];
  array.forEach((item) => {
    nums.push(item);
  });
  return String.fromCharCode.apply(null, nums);
}

async function getCompletionItems(
  uri: Uri,
  character: LanguageInfo["character"]
) {
  const variableContent = await readFileContent(uri);
  const is$ = character === languageTypeMap.sass.character;
  // 匹配注释的正则
  const regx = new RegExp(
    ";[\\s\\S]*?" + `${is$ ? `\\${character}` : character}`,
    "g"
  );

  /** 清除注释、换行、空格 */
  const clearContent = variableContent
    .replace(/[\s\r\n]/g, "")
    .replace(regx, `;${character}`);
  /** 拆分为键值对的变量 */
  const entries = clearContent
    .split(";")
    .filter((item) => item !== "")
    .map((item) => item.split(":"));
  return entries.map((item) => ({
    detail: item[0],
    label: item[1],
    insertText: item[0],
    filterText: item.join(":"),
    kind: CompletionItemKind.Color,
  }));
}

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export async function activate(context: ExtensionContext) {
  console.log("color variable extension is now active!");

  const files = await workspace.findFiles(`package.json`);
  if (!files.length) {
    return;
  }
  const packageJSonContent = await readFileContent(files[0]);
  const jsonObj = JSON.parse(packageJSonContent);

  let languageType: LanguageInfo = {
    ext: "scss",
    character: "$",
  };
  Object.keys(languageTypeMap).map((key: keyof LanguageMap) => {
    if (jsonObj.devDependencies[key]) {
      languageType = languageTypeMap[key];
    }
  });

  /** 单页下的提示项 */
  const completionItems: CompletionItem[] = [];
  /** 存储多页结构下的提示项 */
  // const moduleCompletionMap: Map<string, CompletionItem[]> = new Map();

  /**TODO
   * 1.兼容多页应用下的多个变量声明文件 */
  const config = workspace.getConfiguration();
  const uris = await workspace.findFiles(
    `src/**/${config.fileName || "variable"}.${languageType.ext}`,
    "**/node_modules/**"
  );
  uris.forEach((uri) => {
    getCompletionItems(uri, languageType.character).then((res) => {
      completionItems.push(...res);
    });
  });

  const disposable = languages.registerCompletionItemProvider(
    ["vue", "scss", "less"],
    {
      provideCompletionItems() {
        console.log("execute provider");
        return completionItems;
      },
    }
  );

  context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
