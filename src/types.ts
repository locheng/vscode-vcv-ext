export interface LanguageInfo {
  /** 文件拓展名 */
  ext: string;
  /** 变量声明关键字符 */
  character: string;
}

export interface LanguageMap {
  [key: string]: LanguageInfo;
}
