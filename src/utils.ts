import { LanguageMap } from "./types";

export const languageTypeMap: LanguageMap = {
  sass: {
    ext: "scss",
    character: "$",
  },
  less: {
    ext: "less",
    character: "@",
  },
};
