<h1 align="center">
Color Variables Completion
</h1>

<p>
  颜色变量提示插件，输入颜色值或变量名触发提示。目前支持sass和less，默认的变量声明文件名为variable，可在插件设置中更改，文件中除了变量声明和注释外不能存在其他代码。vscode版本1.73及以上。
</p>
